const hre = require("hardhat");

async function main() {
  const NAME = "Starmint"; // Remplacer par le nom de votre collection
  const CONTRACT_URI = "https://gateway.pinata.cloud/ipfs/QmfZgCjhHVyjqjC3kj1SsCBfLiUK5gVjwftkeXvzCwboMP"; // Remplacer par l'URI de votre contrat
  const DEFAULT_ROYALTIES = 500; // 5%
  const ROYALTIES_RECEIVER_ADDRESS = "0xF02BBa5a85eE0fD193368acF7A814D296EE8DA5e";
  const Starmint = await hre.ethers.getContractFactory("Starmint");

  const starmint = await hre.upgrades.deployProxy(Starmint, [
    NAME,
    DEFAULT_ROYALTIES, // Remplacer par le pourcentage des royalties en basis points (1/10000)
    ROYALTIES_RECEIVER_ADDRESS,
    CONTRACT_URI], {
    initializer: "initialize",
  });

  await starmint.deployed();

  console.log("Starmint contract deployed to:", starmint.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });