import React from 'react';
import Link from 'next/link';

const Footer = () => {
  return (
    <footer className="relative py-12 overflow-hidden bg-black">
      <div className="absolute inset-0">
        <img className="object-cover w-full h-full opacity-50" src="https://landingfoliocom.imgix.net/store/collection/dusk/images/noise.png" alt="" />
      </div>

      <div className="relative px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 gap-y-12 lg:grid-cols-2 xl:grid-cols-7 gap-x-8 xl:gap-x-16">
          <div className="max-w-md mx-auto text-center xl:col-span-3 lg:max-w-sm lg:mx-0 lg:text-left">
            <p className="text-3xl font-normal text-white sm:text-xl">@copyright 2023.</p>
            <div className="relative inline-flex items-center justify-center mt-8 sm:mt-12 group">
              <div className="absolute transition-all duration-200 rounded-md -inset-px bg-gradient-to-r from-cyan-500 to-purple-500 group-hover:shadow-lg group-hover:shadow-cyan-500/50"></div>
              <a href="#" title="" className="relative inline-flex items-center justify-center px-3 text-xs font-normal text-white bg-black border border-transparent rounded-md" role="button"> Rarity AI artists </a>
            </div>
          </div>

          <div className="overflow-hidden lg:-ml-12 lg:-my-8 rounded-xl xl:col-span-4">
            <div className="">
              <div className="w-full">
                <div>
                  <ul className="flex flex-col md:flex-row mt-6 space-y-4 md:space-y-0 md:space-x-4 w-full">
                    <li className="self-center md:self-start">
                      <Link href="/about" title="" className="flex font-normal text-gray-400 transition-all transform hover:text-white duration hover:translate-x-1"> About </Link>
                    </li>

                    <li className="self-center md:self-start">
                      <Link href="/terms" title="" className="flex font-normal text-gray-400 transition-all transform hover:text-white duration hover:translate-x-1"> Terms of use </Link>
                    </li>

                    <li className="self-center md:self-start">
                      <Link href="/support" title="" className="flex font-normal text-gray-400 transition-all transform hover:text-white duration hover:translate-x-1"> Support </Link>
                    </li>

                    <li className="self-center md:self-start">
                      <Link href="/privacy" title="" className="flex font-normal text-gray-400 transition-all transform hover:text-white duration hover:translate-x-1"> Privacy policy </Link>
                    </li>
                  </ul>
                </div>
              </div>

              <hr className="mt-4 border-gray-800" />

              <div className="flex items-center justify-between mt-6">
                <span className="text-lg font-normal text-gray-400"> Follow us on: </span>

                <div className="flex items-center justify-end space-x-4">
                  <a href="https://twitter.com/aiartsnfts" title="" target="_blank" rel="noopener noreferrer" className="inline-flex items-center justify-center w-8 h-8 text-white transition-all bg-transparent rounded-full hover:bg-gray-800 duraiton-200">
                    <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24">
                      <path
                        d="M19.633 7.997c.013.175.013.349.013.523 0 5.325-4.053 11.461-11.46 11.461-2.282 0-4.402-.661-6.186-1.809.324.037.636.05.973.05a8.07 8.07 0 0 0 5.001-1.721 4.036 4.036 0 0 1-3.767-2.793c.249.037.499.062.761.062.361 0 .724-.05 1.061-.137a4.027 4.027 0 0 1-3.23-3.953v-.05c.537.299 1.16.486 1.82.511a4.022 4.022 0 0 1-1.796-3.354c0-.748.199-1.434.548-2.032a11.457 11.457 0 0 0 8.306 4.215c-.062-.3-.1-.611-.1-.923a4.026 4.026 0 0 1 4.028-4.028c1.16 0 2.207.486 2.943 1.272a7.957 7.957 0 0 0 2.556-.973 4.02 4.02 0 0 1-1.771 2.22 8.073 8.073 0 0 0 2.319-.624 8.645 8.645 0 0 1-2.019 2.083z"
                      ></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer;
