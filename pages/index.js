import { useEffect, useState } from 'react';
import { Meta } from '../layouts/Meta';
import Main from '../templates/Main';
import Link from 'next/link';
import InfiniteLoopSlider from '../components/InfiniteLoopSlider';
import { useRouter } from 'next/router';
import NftImageCard from '../components/NftImageCard';
import Image from '../components/Image';

export const cards = [
  {
    "src": "cards/art-by----joachim-wiewaelsci-fi-futuristic--gothic--cityforest--planet--gothic-universe-backro-154589709.png",
    "hash": "LH9G,v?daIDhK$OqnNVrnzskR%WF",
    "class": "mt-4 rotate-3",
    "author": "JENS OHL"
  },
  {
    "src": "cards/frecklesclear-bright-eyes-prompt-design-by-nomeradona-534302474.png",
    "hash": "L8JZ_7570#~W~WM{00x]01RjrXt6",
    "class": "mt-6 -rotate-2",
    "author": "Nomeradona"
  },
  {
    "src": "cards/dreamlikeart--splash-artstation-intricate-details-masterpiece-digital-art----436544375.png",
    "hash": "L5A,eJ0J4T.T0-=f-WaO0c~EAbrD",
    "class": "mt-5 -rotate-[3]",
    "author": "Sergey Tert"
  },
  {
    "src": "cards/a-smiling-of-the-dnd-character-tiny-gremlin-face-on-the-spring-garden-background-in-dynamic-ex-962667314.png",
    "hash": "L6D[:r~84X0O0}9d069wO%^#-U=^",
    "class": "mt-8 rotate-3",
    "author": "DEPOST"
  },
  {
    "src": "cards/abstract-black-oil-kids-story-book-style-soft-impressionist-stained-glass-background-ice-cubes--498122759.png",
    "hash": "L94{G4X:Q,R3yZtSR4Mwpxx^MvMw",
    "class": "mt-3 rotate-[5deg]",
    "author": "Kind Fairy"
  },
  {
    "src": "cards/steampunkitty-by-bella-426471203.png",
    "hash": "LEF#aM0ME2S#~U58-Ut69GIq-poe",
    "class": "mt-3 rotate-[5deg]",
    "author": "Bella"
  },
  {
    "src": "cards/robot-digital-illustration-comic-style-stunning-background-perfect-anatomy-centered-approaching-423081967.png",
    "hash": "L9GR#sBW00000x;u9h?K4U.9vJNY",
    "class": "mt-4 rotate-[3deg]",
    "author": "pokemon may S"
  },
  {
    "src": "cards/oliva-wilde-sad-girl-in-rain-flat-stomach-detailed-eyes-digital-illustration--comic-style-108196376.png",
    "hash": "L$LgX^Rj~qt6%2NGWWt6bI%Ls:M|",
    "class": "mt-8 -rotate-3",
    "author": "MACEK"
  },
  {
    "src": "cards/playground-rpg--cute-little-troll-creature-in-woods---low-angle-realistic-anime-style-sharp--763537888.png",
    "hash": "L57dnFW94mxwp3n#MutSOAn$wIbd",
    "class": "mt-6 rotate-[1deg]",
    "author": "Andromeda"
  },
  {
    "src": "cards/prfm-style-a-silverback-gorilla-with-a-cowboy-hat-playing-blues-guitar-in-the-style-of-a-t-shirt-de-282366670.png",
    "hash": "LRJ[Cot7~qNG%MayM{WB-;kCMxn~",
    "class": "mt-8 rotate-3",
    "author": "Bob from france"
  }
];

const Index = () => {
  const router = useRouter();
  const duration = 100000;
  const [nftsCards, setNftsCards] = useState([]);

  const random = (min, max) => Math.floor(Math.random() * (max - min)) + min;

  useEffect(() => {
    let index = 0,
      interval = 1000;

    const rand = (min, max) =>
      Math.floor(Math.random() * (max - min + 1)) + min;

    const animate = star => {
      star.style.setProperty("--star-left", `${rand(-10, 100)}%`);
      star.style.setProperty("--star-top", `${rand(-40, 80)}%`);

      star.style.animation = "none";
      star.offsetHeight;
      star.style.animation = "";
    }

    for (const star of document.getElementsByClassName("magic-star")) {
      setTimeout(() => {
        animate(star);

        setInterval(() => animate(star), 1000);
      }, index++ * (interval / 3))
    }
  }, [])

  useEffect(() => {
    setNftsCards(cards);
  }, []);

  return (
    <Main>
      <Meta
        title="Rarity AI Artists"
        description="Where the talents of ai proudly create masterpieces"
      />
      <div className="absolute inset-0">
        <Image
          src="/background.png"
          loader="tiny.png"
          alt="banner"
          title=""
          className="object-cover w-full h-full"
        />
      </div>

      <section className="relative py-12 sm:py-16 lg:pb-20 xl:pb-24">
        <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
          <div className="max-w-lg mx-auto text-center">
            <h1 className="text-4xl font-bold text-gray-900 uppercase sm:text-5xl lg:text-6xl">Welcome to
              <span className="magic">
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z" />
                  </svg>
                </span>
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z" />
                  </svg>
                </span>
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z" />
                  </svg>
                </span>
                <span className="magic-text">RARITY AI ARTISTS</span>
              </span>
            </h1>
            <p className="mt-6 text-lg font-normal text-gray-600 lg:text-xl">Where the talents of ai proudly create masterpieces.</p>

            <div className="flex flex-col px-12 mt-10 space-y-5 sm:px-0 sm:space-y-0 sm:space-x-5 sm:flex-row sm:items-center sm:justify-center">
              <a
                href="https://discord.gg/aUxXC2tvFH"
                title="Join Discord"
                className="relative inline-flex items-center justify-center pl-16 pr-8 py-3 text-base font-semibold text-gray-900 transition-all duration-200 border border-gray-900 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900 hover:bg-gray-900 hover:text-white"
                role="button"
              >
                <div className="absolute inset-y-0 left-0 flex items-center pl-4">
                  <svg className="mr-5" width="24px" height="24px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="currentColor"><path d="M5.5 16c5 2.5 8 2.5 13 0M15.5 17.5l1 2s4.171-1.328 5.5-3.5c0-1 .53-8.147-3-10.5-1.5-1-4-1.5-4-1.5l-1 2h-2" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path><path d="M8.528 17.5l-1 2s-4.171-1.328-5.5-3.5c0-1-.53-8.147 3-10.5 1.5-1 4-1.5 4-1.5l1 2h2" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path><path d="M8.5 14c-.828 0-1.5-.895-1.5-2s.672-2 1.5-2 1.5.895 1.5 2-.672 2-1.5 2zM15.5 14c-.828 0-1.5-.895-1.5-2s.672-2 1.5-2 1.5.895 1.5 2-.672 2-1.5 2z" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path></svg>
                </div>
                Join Discord
              </a>

              <a
                href="#"
                title="See on OpenSea"
                className="relative inline-flex items-center justify-center py-3 pl-16 pr-8 text-base font-bold text-white transition-all duration-200 bg-blue-600 border border-blue-600 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-900 hover:bg-blue-700"
                role="button"
              >
                <div className="absolute inset-y-0 left-0 flex items-center pl-4">
                  <svg className="w-8 h-8" viewBox="0 0 90 90" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M22.2011 46.512L22.3952 46.2069L34.1015 27.8939C34.2726 27.6257 34.6748 27.6535 34.8043 27.9447C36.7599 32.3277 38.4475 37.7786 37.6569 41.1721C37.3194 42.5683 36.3947 44.4593 35.3544 46.2069C35.2204 46.4612 35.0724 46.7109 34.9152 46.9513C34.8413 47.0622 34.7164 47.127 34.5823 47.127H22.5432C22.2195 47.127 22.03 46.7756 22.2011 46.512Z"
                    />
                    <path
                      d="M74.38 49.9149V52.8137C74.38 52.9801 74.2783 53.1281 74.1303 53.1928C73.2242 53.5812 70.1219 55.0052 68.832 56.799C65.5402 61.3807 63.0251 67.932 57.4031 67.932H33.9489C25.6362 67.932 18.9 61.1727 18.9 52.8322V52.564C18.9 52.3421 19.0803 52.1618 19.3022 52.1618H32.377C32.6359 52.1618 32.8255 52.4022 32.8024 52.6565C32.7099 53.5072 32.8671 54.3764 33.2693 55.167C34.046 56.7435 35.655 57.7283 37.3933 57.7283H43.866V52.675H37.4673C37.139 52.675 36.9449 52.2959 37.1344 52.0277C37.2038 51.9214 37.2824 51.8104 37.3656 51.6856C37.9712 50.8257 38.8358 49.4895 39.6957 47.9684C40.2829 46.9421 40.8516 45.8463 41.3093 44.746C41.4018 44.5472 41.4757 44.3438 41.5497 44.1449C41.6745 43.7936 41.804 43.4653 41.8964 43.1371C41.9889 42.8597 42.0629 42.5684 42.1369 42.2956C42.3542 41.3617 42.4466 40.3723 42.4466 39.3459C42.4466 38.9437 42.4281 38.523 42.3911 38.1207C42.3727 37.6815 42.3172 37.2423 42.2617 36.8031C42.2247 36.4147 42.1554 36.031 42.0814 35.6288C41.9889 35.0416 41.8595 34.4591 41.7115 33.8719L41.6607 33.65C41.5497 33.2478 41.4572 32.864 41.3278 32.4618C40.9625 31.1996 40.5418 29.9698 40.098 28.8186C39.9362 28.3609 39.7512 27.9217 39.5663 27.4825C39.2935 26.8213 39.0161 26.2203 38.7618 25.6516C38.6324 25.3927 38.5214 25.1569 38.4105 24.9165C38.2856 24.6437 38.1562 24.371 38.0267 24.112C37.9343 23.9132 37.8279 23.7283 37.7539 23.5434L36.9634 22.0824C36.8524 21.8836 37.0373 21.6478 37.2546 21.7079L42.2016 23.0487H42.2155C42.2247 23.0487 42.2293 23.0533 42.234 23.0533L42.8858 23.2336L43.6025 23.437L43.866 23.511V20.5706C43.866 19.1512 45.0033 18 46.4088 18C47.1116 18 47.7496 18.2866 48.2073 18.7536C48.665 19.2206 48.9517 19.8586 48.9517 20.5706V24.935L49.4787 25.0829C49.5203 25.0968 49.5619 25.1153 49.5989 25.143C49.7284 25.2401 49.9133 25.3835 50.1491 25.5591C50.334 25.7071 50.5328 25.8874 50.7733 26.0723C51.2495 26.4561 51.8181 26.9508 52.4423 27.5194C52.6087 27.6628 52.7705 27.8107 52.9185 27.9587C53.7229 28.7076 54.6245 29.5861 55.4844 30.557C55.7248 30.8297 55.9606 31.1071 56.201 31.3984C56.4415 31.6943 56.6957 31.9856 56.9177 32.2769C57.2089 32.6652 57.5233 33.0674 57.7961 33.4882C57.9255 33.687 58.0735 33.8904 58.1983 34.0892C58.5497 34.6209 58.8595 35.1711 59.1554 35.7212C59.2802 35.9755 59.4096 36.2529 59.5206 36.5257C59.8488 37.2608 60.1078 38.0098 60.2742 38.7588C60.3251 38.9206 60.362 39.0963 60.3805 39.2535V39.2904C60.436 39.5124 60.4545 39.7482 60.473 39.9886C60.547 40.756 60.51 41.5235 60.3435 42.2956C60.2742 42.6239 60.1817 42.9336 60.0708 43.2619C59.9598 43.5763 59.8488 43.9045 59.7055 44.2143C59.4281 44.8569 59.0999 45.4996 58.7115 46.1006C58.5867 46.3225 58.4387 46.5583 58.2908 46.7802C58.129 47.016 57.9625 47.238 57.8146 47.4553C57.6112 47.7327 57.3939 48.0239 57.1719 48.2828C56.9731 48.5556 56.7697 48.8284 56.5478 49.0688C56.238 49.434 55.9421 49.7808 55.6324 50.1137C55.4474 50.331 55.2486 50.5529 55.0452 50.7517C54.8464 50.9736 54.643 51.1724 54.458 51.3573C54.1483 51.6671 53.8894 51.9075 53.6721 52.1063L53.1635 52.5733C53.0895 52.638 52.9924 52.675 52.8907 52.675H48.9517V57.7283H53.9079C55.0175 57.7283 56.0716 57.3353 56.9223 56.6141C57.2135 56.3598 58.485 55.2594 59.9876 53.5997C60.0384 53.5442 60.1031 53.5026 60.1771 53.4841L73.8668 49.5265C74.1211 49.4525 74.38 49.6467 74.38 49.9149Z"
                    />
                  </svg>
                </div>
                See on Opensea
              </a>
            </div>
          </div>
        </div>
      </section>

      <div className="">
        <InfiniteLoopSlider duration={random(duration - 5000, duration + 5000)} reverse="false">
          {nftsCards.map(img => (
            <NftImageCard key={img.src} data={img} router={router} />
          ))}
        </InfiniteLoopSlider>
      </div>

      <section className="py-12 bg-gray-50 sm:py-16 lg:py-20">
        <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
          <div className="max-w-md mx-auto text-center">
            <h2 className="text-3xl font-bold text-gray-900">How is work</h2>
            <p className="mt-4 text-base font-medium text-gray-500">
              We are a community of artists, developers, and crypto enthusiasts. We are building a new way to experience art, music, and culture, powered by blockchain technology.
            </p>
          </div>

          <div className="grid grid-cols-1 mt-12 gap-x-6 gap-y-10 sm:mt-16 lg:mt-20 sm:grid-cols-2 lg:grid-cols-4">
            <div>
              <p className="text-sm font-semibold tracking-wide text-gray-400 uppercase">Phase 1</p>

              <div className="relative mt-2">
                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                  <div className="w-full border-t border-gray-300 border-dashed"></div>
                </div>
                <div className="relative flex justify-start">
                  <span className="pr-5 text-xl font-bold text-gray-900 bg-gray-50"> Join </span>
                </div>
              </div>

              <div className="mt-6 overflow-hidden bg-white rounded-lg">
                <div className="px-4 py-5 sm:p-6">
                  <ul className="space-y-3">

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Connect with your discord </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Connect your metamask </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Join the discord community </span>
                    </li>

                  </ul>
                </div>
              </div>
            </div>

            <div>
              <p className="text-sm font-semibold tracking-wide text-gray-400 uppercase">Phase 2</p>
              <div className="relative mt-2">
                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                  <div className="w-full border-t border-gray-300 border-dashed"></div>
                </div>
                <div className="relative flex justify-start">
                  <span className="pr-5 text-xl font-bold text-gray-900 bg-gray-50"> Create </span>
                </div>
              </div>

              <div className="mt-6 overflow-hidden bg-white rounded-lg">
                <div className="px-4 py-5 sm:p-6">
                  <ul className="space-y-3">
                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Create your NFT </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Mint </span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div>
              <p className="text-sm font-semibold tracking-wide text-gray-400 uppercase">Phase 3</p>

              <div className="relative mt-2">
                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                  <div className="w-full border-t border-gray-300 border-dashed"></div>
                </div>
                <div className="relative flex justify-start">
                  <span className="pr-5 text-xl font-bold text-gray-900 bg-gray-50"> Sell </span>
                </div>
              </div>

              <div className="mt-6 overflow-hidden bg-white rounded-lg">
                <div className="px-4 py-5 sm:p-6">
                  <ul className="space-y-3">
                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                      </svg>

                      <span className="ml-2 text-base font-medium text-gray-900"> listing your NFT on Opensea or Rarity </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Let&apos;s people own your NFTS </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Let&apos;s people discover your talent </span>
                    </li>

                  </ul>
                </div>
              </div>
            </div>

            <div>
              <p className="text-sm font-semibold tracking-wide text-gray-400 uppercase">Phase 4</p>

              <div className="relative mt-2">
                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                  <div className="w-full border-t border-gray-300 border-dashed"></div>
                </div>
                <div className="relative flex justify-start">
                  <span className="pr-5 text-xl font-bold text-gray-900 bg-gray-50"> Share and Growth </span>
                </div>
              </div>

              <div className="mt-6 overflow-hidden bg-white rounded-lg">
                <div className="px-4 py-5 sm:p-6">
                  <ul className="space-y-3">
                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Share with the community </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Perform your skills and your style to be unique </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Share on social medias </span>
                    </li>

                    <li className="flex items-center">
                      <svg className="w-5 h-5 text-gray-900 shrink-0" stroke="currentColor" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.5" cy="11.5" r="8.75" strokeWidth="1.5" />
                      </svg>
                      <span className="ml-2 text-base font-medium text-gray-900"> Enlarge the collection by creating more.. </span>
                    </li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="relative py-12 bg-white sm:py-16 lg:py-20">
        <div className="relative px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
          <div className="max-w-md mx-auto text-center lg:max-w-lg">
            <h1 className="text-3xl font-bold text-gray-900 sm:text-4xl lg:text-5xl">
              Discover, Collect & Sell Incredible AI NFTs
            </h1>
            <p className="mt-4 text-lg font-normal text-gray-500 sm:text-xl">
              A listing of the best AI Artists NFTs in the world.
            </p>
          </div>

          <div className="mt-12">
            <div className="flex w-full gap-6 py-4 mx-auto -my-4 overflow-x-auto md:justify-center">
              <div
                className="overflow-hidden w-72 transition-all duration-200 transform bg-white border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                  <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-1.png" alt="" />
                </a>
                <div className="p-4">
                  <div className="flex items-center justify-between space-x-6">
                    <p className="flex-1 text-base font-medium text-gray-900">
                      <a href="#" title="">
                        Faratey58
                      </a>
                    </p>
                    <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                      <div className="absolute inset-0">
                        <img className="w-full h-full object-coveer"
                          src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                      </div>
                      <div className="relative text-xs font-bold text-white uppercase">
                        FA
                      </div>
                    </a>
                  </div>
                </div>
                <div className="p-4 border-t border-gray-200">
                  <div className="flex items-center justify-between">
                    <p className="text-sm font-medium text-gray-500">
                      Price
                    </p>
                    <p className="text-sm font-medium text-gray-900">
                      1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                    </p>
                  </div>
                </div>
              </div>

              <div
                className="overflow-hidden w-72 transition-all duration-200 transform bg-white border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                  <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-2.png" alt="" />
                </a>
                <div className="p-4">
                  <div className="flex items-center justify-between space-x-6">
                    <p className="flex-1 text-base font-medium text-gray-900">
                      <a href="#" title="">
                        Wildwildworld
                      </a>
                    </p>
                    <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                      <div className="absolute inset-0">
                        <img className="w-full h-full object-coveer"
                          src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                      </div>
                      <div className="relative text-xs font-bold text-white uppercase">
                        Wd
                      </div>
                    </a>
                  </div>
                </div>

                <div className="p-4 border-t border-gray-200">
                  <div className="flex items-center justify-between">
                    <p className="text-sm font-medium text-gray-500">
                      Price
                    </p>
                    <p className="text-sm font-medium text-gray-900">
                      1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                    </p>
                  </div>
                </div>
              </div>

              <div
                className="overflow-hidden w-72 transition-all duration-200 transform bg-white border border-gray-200 rounded-md snap-start scroll-ml-6 shrink-0 hover:shadow-lg hover:-translate-y-1 group">
                <a href="#" title="" className="block overflow-hidden aspect-w-1 aspect-h-1">
                  <img className="object-cover w-full h-full transition-all duration-200 group-hover:scale-110"
                    src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/hero-single/2/image-3.png" alt="" />
                </a>
                <div className="p-4">
                  <div className="flex items-center justify-between space-x-6">
                    <p className="flex-1 text-base font-medium text-gray-900">
                      <a href="#" title="">
                        ZaTaS
                      </a>
                    </p>
                    <a href="#" title="" className="relative inline-flex items-center justify-center shrink-0 w-7 h-7">
                      <div className="absolute inset-0">
                        <img className="w-full h-full object-coveer"
                          src="https://landingfoliocom.imgix.net/store/collection/niftyui/images/featured-drops-marketplace/9/avatar-background.png" alt="" />
                      </div>
                      <div className="relative text-xs font-bold text-white uppercase">
                        ZS
                      </div>
                    </a>
                  </div>
                </div>

                <div className="p-4 border-t border-gray-200">
                  <div className="flex items-center justify-between">
                    <p className="text-sm font-medium text-gray-500">
                      Price
                    </p>
                    <p className="text-sm font-medium text-gray-900">
                      1.4 MATIC <span className="text-gray-500">(2,352 USD)</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mt-8 text-center">
            <Link href="/thecollection" title=""
              className="pb-2 text-sm font-medium text-gray-900 transition-all duration-200 border-b border-gray-900">
              Explore all NFTs
            </Link>
          </div>
        </div>
      </section>

    </Main>

  )
}

export default Index
