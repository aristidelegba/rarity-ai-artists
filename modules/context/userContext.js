import { createContext, useEffect, useState } from "react";
import { getLoggedInUser } from "../http/auth";

export const userContext = createContext({
  user: undefined,
  setUser: () => { }
});

export const UserContextProvider = ({ children }) => {

  const [user, setUser] = useState();

  useEffect(() => {
    (async () => {
      const user = await getLoggedInUser();
      if (user) setUser(user);
    })();
  }, []);

  return (
    <userContext.Provider value={{ user, setUser }}>
      {children}
    </userContext.Provider>
  );
}
